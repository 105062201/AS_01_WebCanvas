
var x = 0;
var y = 0;
var startx;
var starty;
var size=1;
var draw_pen=true;//1
var draw_square=false;//2
var draw_circle=false;//3
var draw_text=false;//4
var draw_eraser=false;//5
var draw_tri=false;//6
var draw_circle_sp=false;//8
var draw_tri_sp=false;//9
var draw_rec_sp=false;//10
var funcType=1;
var ctx;
var _canvas;
var initCanvas;
var font;
var Input_or_not = false;
var canvas_width=750;
var canvas_height=500;
var cPushArray = new Array();
var cStep = -1;
var isDrawing = true;
var tri_pen=false;
var start_circle = true;
var start_tri = true;
var start_rec = true;

_canvas = document.getElementById('canvas');
ctx = _canvas.getContext('2d'); 
_canvas.style.cursor = "copy";
var cPushArray = new Array();
var cStep = -1;


//決定功能
function changefunction(funcType){
    if(funcType==1){ 
        draw_pen=true;
        draw_square=false;
        draw_circle=false;
        draw_text=false;
        draw_eraser=false;
        draw_tri=false;
        draw_circle_sp=false;
        draw_tri_sp=false;
        draw_rec_sp=false;
        isDrawing=true;
    }
    else if(funcType==2){
        draw_pen=false;
        draw_square=true;
        draw_circle=false;
        draw_text=false;
        draw_eraser=false;
        draw_tri=false;
        draw_circle_sp=false;
        draw_tri_sp=false;
        draw_rec_sp=false;
        isDrawing=true;
    }
    else if(funcType==3){
        draw_pen=false;
        draw_square=false;
        draw_circle=true;
        draw_text=false;
        draw_eraser=false;
        draw_tri=false;
        draw_circle_sp=false;
        draw_tri_sp=false;
        draw_rec_sp=false;
        isDrawing=true;
    }
    else if(funcType==4){
        draw_pen=false;
        draw_square=false;
        draw_circle=false;
        draw_text=true;
        draw_eraser=false;
        draw_tri=false;
        draw_circle_sp=false;
        draw_tri_sp=false;
        draw_rec_sp=false;
        isDrawing=false;
    }
    else if(funcType==5){
        draw_pen=false;
        draw_square=false;
        draw_circle=false;
        draw_text=false;
        draw_eraser=true;
        draw_tri=false;
        draw_circle_sp=false;
        draw_tri_sp=false;
        draw_rec_sp=false;
        isDrawing=true;
    }
    else if(funcType==6){
        draw_pen=false;
        draw_square=false;
        draw_circle=false;
        draw_text=false;
        draw_eraser=false;
        draw_tri=true;
        draw_circle_sp=false;
        draw_tri_sp=false;
        draw_rec_sp=false;
        isDrawing=true;
    }
    else if(funcType==8){
        draw_pen=false;
        draw_square=false;
        draw_circle=false;
        draw_text=false;
        draw_eraser=false;
        draw_tri=false;
        draw_circle_sp=true;
        draw_tri_sp=false;
        draw_rec_sp=false;
        isDrawing=true;
    }
    else if(funcType==9){
        draw_pen=false;
        draw_square=false;
        draw_circle=false;
        draw_text=false;
        draw_eraser=false;
        draw_tri=false;
        draw_circle_sp=false;
        draw_tri_sp=true;
        draw_rec_sp=false;
        isDrawing=true;
    }
    else if(funcType==10){
        draw_pen=false;
        draw_square=false;
        draw_circle=false;
        draw_text=false;
        draw_eraser=false;
        draw_tri=false;
        draw_circle_sp=false;
        draw_tri_sp=false;
        draw_rec_sp=true;
        isDrawing=true;
    }
    else {
        draw_pen=false;
        draw_square=false;
        draw_circle=false;
        draw_text=false;
        draw_eraser=false;
        draw_tri=false;
        draw_circle_sp=false;
        draw_tri_sp=false;
        draw_rec_sp=false;
        isDrawing=false;
    }
    changecursor(funcType);
}
//復原與重作
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('canvas').toDataURL());
    //document.title = cStep + ":" + cPushArray.length;
}
//重作
function Undo() {
    if (cStep > -1) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        ctx.clearRect(0,0,canvas_width,canvas_height);
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
        //document.title = cStep + ":" + cPushArray.length;
    }
}
//復原
function Redo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        ctx.clearRect(0,0,canvas_width,canvas_height);
        canvasPic.onload = function (){ctx.drawImage(canvasPic, 0, 0);}
        //document.title = cStep + ":" + cPushArray.length;
    }
}




//清空畫布
function clearall(){
    canvas.width=canvas_width;
    canvas.height=canvas_height;
    cPushArray=[];
    cStep=-1;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

//變換鼠標
function changecursor(funcType){
    if(funcType==4) _canvas.style.cursor = "text";
    else if(funcType==2||funcType==3||funcType==6) _canvas.style.cursor = "crosshair";
    else if(funcType==5)_canvas.style.cursor = "cell";
    else if(funcType==1||funcType==8||funcType==9)_canvas.style.cursor = "copy";
    else _canvas.style.cursor = "default";
}


//變換brush樣式
function Brush_onclick(){
    if(document.getElementById("brushSelect").value == "1"){
        ctx.lineCap="butt";
    }
    else if(document.getElementById("brushSelect").value == "2"){
        ctx.lineCap="square";
    }
    else if(document.getElementById("brushSelect").value == "3") {
        ctx.lineCap="round";
    }
}

//輸入文字
function Button_onclick(){
    if(document.getElementById("mySelect").value == "6"){
        font = '6px sans-serif';
    }
    else if(document.getElementById("mySelect").value == "8"){
        font = '8px sans-serif';
    }
    else if(document.getElementById("mySelect").value == "10"){
        font = '10px sans-serif';
    }
    else if(document.getElementById("mySelect").value == "12"){
        font = '12px sans-serif';
    }
    else if(document.getElementById("mySelect").value == "14"){
        font = '14px sans-serif';
    }
    else if(document.getElementById("mySelect").value == "16"){
        font = '16px sans-serif';
    }
    else if(document.getElementById("mySelect").value == "18"){
        font = '18px sans-serif';
    }
    else if(document.getElementById("mySelect").value == "20"){
        font = '20px sans-serif';
    }
    else if(document.getElementById("mySelect").value == "24"){
        font = '24px sans-serif';
    }
    else if(document.getElementById("mySelect").value == "28"){
        font = '28px sans-serif';
    }
    else if(document.getElementById("mySelect").value == "40"){
        font = '40px sans-serif';
    }
    else if(document.getElementById("mySelect").value == "60"){
        font = '60px sans-serif';
    }
    draw = false;
    _canvas.onclick = function(e) {
    if (Input_or_not) return;
        addInput(e.clientX, e.clientY);
    }
    draw = false;
    _canvas.onclick = function(e) {
    if (Input_or_not||draw_text==false) return;
    addInput(e.clientX, e.clientY);
    }
}
function addInput(x, y) {
    var input = document.createElement('input');
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x - 4) + 'px';
    input.style.top = (y - 4) + 'px';
    input.onkeydown = handleEnter;
    document.body.appendChild(input);
    input.focus();
    Input_or_not = true;
}
function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        Input_or_not = false;
    }
}
function drawText(txt, x, y) {
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = font;
    ctx.fillText(txt, x, y-430);
    cPush();
}


//上傳圖片
function previewFile() {
    var file    = document.querySelector('input[type=file]').files[0];
    var reader  = new FileReader();
  
    reader.addEventListener("load", function () {
        loadImageURL(ctx, reader.result);
    }, false);
        reader.readAsDataURL(file);
}
function loadImageURL(ctx, url)  {
    var image = document.createElement('img');
    image.addEventListener('load', function() {
        var color = ctx.fillStyle, size = ctx.lineWidth;
        ctx.canvas.width = image.width;
        ctx.canvas.height = image.height;
        ctx.drawImage(image, 0, 0);
        initCanvas = _canvas.toDataURL();
        ctx.fillStyle = color;
        //ctx.strokeStyle = color;
        ctx.lineWidth = size;
    });
    image.src = url;
    ;
}  
    
//畫筆顏色
$('.color').change(function(){
    r = $('#red').val();
    g = $('#green').val();
    b = $('#blue').val();
    changeColor(r,g,b);
});
function changeColor(r,g,b){
    colors = {
        red : r,
        green : g,
        blue : b
    }
    $.each(colors, function(_color, _value) {
        $('#v'+_color).val(_value);
    });
    ctx.strokeStyle = "rgb("+r+","+g+","+b+")" ;
};
    
        
    //畫筆粗細
    $('.size').change(function(){
        size = $('#brush').val();
        changebrush(size);
    });
    function changebrush(size){
        ctx.lineWidth =size;
    };


      
    //載入畫布
    function loadCanvas(){
        var img = new Image();
        img.src = initCanvas;
        ctx.drawImage(img,0,0,ctx.canvas.width,ctx.canvas.height);    
    }

    //得到滑鼠現在XY
    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        //getBoundingClientRect 取得物件完整座標資訊，包含寬高等
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };   
    }
    
    //mouseMove event
    function mouseMove(evt) {
        var mousePos = getMousePos(_canvas, evt);
        //透過getMousePos function 去取得滑鼠座標
        //mousePos 是一個物件，包含x和y的值
        if(draw_pen){
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.stroke();
        }
        
        else if(draw_square){
            var width = mousePos.x - startx;
            var height = mousePos.y - starty;
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            loadCanvas();
            ctx.beginPath();
            ctx.strokeRect(startx, starty, width, height);
        }
        else if(draw_circle){
            var width = mousePos.x - startx;
            var radius=width;
            if(radius<0) radius=radius*(-1);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            loadCanvas();
            ctx.beginPath();
            ctx.arc(mousePos.x,mousePos.y,radius,0,2*Math.PI,true);
            ctx.stroke();
        }
        else if(draw_eraser){
            ctx.clearRect(mousePos.x,mousePos.y,ctx.lineWidth+3,ctx.lineWidth+3);
        }
        else if(draw_tri){
            var width = mousePos.x - startx;
            var height = mousePos.y - starty;
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            loadCanvas();
            ctx.beginPath();
            ctx.moveTo(startx+0.5*width,starty);
            ctx.lineTo(startx,starty+height);
            ctx.lineTo(startx+width,starty+height);
            ctx.closePath();
            ctx.stroke();
        }
        else if(draw_tri_sp){
            var width = 50;
            var height = 50;
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            loadCanvas();
            if(start_tri){
                ctx.beginPath();
                start_tri=false;
            }
            ctx.moveTo(mousePos.x,mousePos.y-0.5*height);
            ctx.lineTo(mousePos.x-0.5*width,mousePos.y+0.2*height);
            ctx.lineTo(mousePos.x+0.5*width,mousePos.y+0.2*height);
            //ctx.lineTo(startx+0.5*width,starty);
            ctx.closePath();
            ctx.stroke();
        }
        else if(draw_circle_sp){
            //var width = mousePos.x - startx;
            var radius=20;
            if(radius<0) radius=radius*(-1);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            loadCanvas();
            if(start_circle){
                ctx.beginPath();
                start_circle=false;
            }
            ctx.arc(mousePos.x,mousePos.y,radius,0,2*Math.PI,true);
            ctx.stroke();
        }
        else if(draw_rec_sp){
            var width = 25;
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            loadCanvas();
            if(start_rec){
                ctx.beginPath();
                start_rec=false;
            }
            ctx.moveTo(mousePos.x-15,mousePos.y-15);
            ctx.lineTo(mousePos.x-15,mousePos.y+15);
            ctx.lineTo(mousePos.x+15,mousePos.y+15);
            ctx.lineTo(mousePos.x+15,mousePos.y-15);
            ctx.closePath();
            ctx.stroke();
        }
        
    };

    //mouseDown event
    canvas.addEventListener('mousedown', function(evt) {
        var tmpR = document.getElementById("red").value;
        var tmpG = document.getElementById("green").value;
        var tmpB = document.getElementById("blue").value;
        ctx.strokeStyle = "rgb("+tmpR+","+tmpG+","+tmpB+")"
        var mousePos = getMousePos(_canvas, evt);
        startx=mousePos.x;
        starty=mousePos.y;
        //從按下去就會執行第一次的座標取得
        evt.preventDefault();
        if(draw_pen)ctx.beginPath();
        ctx.moveTo(mousePos.x, mousePos.y);  
        //每次的點用moveTo去區別開，如果用lineTo會連在一起  
        canvas.addEventListener('mousemove', mouseMove, false);
    });

    //mouseUp event
    canvas.addEventListener('mouseup', function() {
        initCanvas = _canvas.toDataURL();
        if(isDrawing)cPush();
        if(draw_circle_sp&&!start_circle)start_circle=true;
        if(draw_tri_sp&&!start_tri)start_tri=true;
        if(draw_rec_sp&&!start_rec)start_rec=true;
        //存進陣列
        canvas.removeEventListener('mousemove', mouseMove, false);
        }, false);
        //如果滑鼠放開，將會停止mouseup的偵聽

