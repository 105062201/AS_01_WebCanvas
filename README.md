# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="mainpage.png" width="500px" height="700px"></img>

## Tool Discription
**Basic component**
1. Basic control tools
    * Brush and eraser
    * Color selector----可透過拉動RGB色調來調整，下方數字僅用作顯示器，顯示當前range之值，直接改動其數值並無法改動RGB
    * Simple menu(brush size)----可調整線條粗細或是下方圖形繪製和特殊筆刷的邊框粗細，注意調整完粗細要記得按一下該畫筆(或圖形)功能，才會實作
2. Text input----請先選擇欲寫的大小，再點下start text，接著在canvas內點一處欲書寫的位置，書寫完後按ENTER即可完成輸入
3. Cursor icon----畫線或用特殊筆刷的時候，cursor旁會有加號，拉圖形時為十字，打字時為text的cursor
4. Refresh button----點下clear all可恢復預設canvas

**Advance tools**
1. Different brush shapes----筆頭形狀僅作了圓形與方形，選完筆頭後請務必在點一下欲使用的筆刷樣式以更新筆頭
2. Un/Re-do button----undo恢復上一步，redo恢復下一步
3. Image tool----可上傳圖片作為背景，繼續在上面繪圖，並會隨你圖片大小改變canvas大小
4. Download----Save Canvas To Local(JPG)

**Other useful widgets**
1. 圖形繪製----可繪製空心的三角形、圓形、矩形，隨意拉扯其大小(若將三角形的brush size調大一點就成了上方未直接實作的三角形筆頭形狀了)(圓形圓心訂在當下滑鼠位置上)
2. 特殊筆刷----可留下連續殘影的空心三角形、圓形、矩形的筆刷，注意brush size越小效果將比較明顯

